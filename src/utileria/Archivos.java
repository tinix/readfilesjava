package utileria;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author tinix
 */
public class Archivos {
    // CrearArchivo
    public static void crearArchivo(String nombreArchivo) {
        File archivo = new File(nombreArchivo);
        try {
            PrintWriter salida = new  PrintWriter(new FileWriter(archivo));
            salida.close();
            System.out.println("El Archivo se a creado correctamente\n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    // EscribirArchivo
    public static void escribirArchivo(String nombreArchivo) {
        File archivo = new File(nombreArchivo);
        try {
            PrintWriter salida = new PrintWriter(new FileWriter(archivo));
                    String contenido = "Contenido a escribir en el archivo";
                    salida.println(contenido);
                    salida.println();
                    salida.println("Fin de Escritura");
                    salida.close();
                    System.out.println("Se ha escrito correctamente \n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    // Leer Archivo
    public static void leerArchivo(String nombreArchivo) {
        File archivo = new File(nombreArchivo);
        try {
            BufferedReader entrada = new BufferedReader(new FileReader(archivo));
            String lectura;
            lectura = entrada.readLine();
            while (lectura != null ) {
                System.out.println("Lectura :" + lectura);
                lectura = entrada.readLine();  
            }
            entrada.close();
        }
        
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    
    public static void anexarArchivo(String nombreArchivo){
        File archivo = new File(nombreArchivo);
        try {
            PrintWriter salida = new PrintWriter(new FileWriter(archivo, true));
            String contenido = "Anexando informacion al archivo";
            salida.println(contenido);
            salida.println();
            salida.println("fin de anexar");
            salida.close();
            System.out.println("Se ha anexado el informacion correctamente \n");
        } catch (IOException ex) {
        	ex.printStackTrace();
        } 
    }
}
